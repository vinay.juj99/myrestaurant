// const bodyParser = require("body-parser")
import bodyParser from 'body-parser';
// const express = require("express")
import express from 'express';
//Creating an instance of express
const app = express();

// const mongoose = require("mongoose")

import mongoose from "mongoose"



//Returns middleware that only parses json
app.use(bodyParser.json());

//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

import cors from 'cors'

app.use(cors({
    origin:"*"
}))

console.log("hello world")

import{
       get_user_data_handler,
       get_restaurant_details_handler,
       menu_data_handler,
       } 
       from './pathhandlers/getApi.js'

// importing the login_user_handler and register_user_handler from the postapis file
import{
    login_user_handler,
    register_user_handler
    }
     from './pathhandlers/postApi.js'

// importing the forgot_user_handler from the putapis file
import{
    forgot_user_handler
    }
    from './pathhandlers/putApi.js'

// getting user_data from db and sending it to client
app.get("/user_data",get_user_data_handler)


// getting user_data from db and sending it to client
app.get("/restaurant_details", get_restaurant_details_handler)

// getting user_data from db and sending it to client
app.get("/menudata",menu_data_handler)




// getiing the user data from client and store it into database
app.post("/register", register_user_handler)

// validating the user data and given authentication
app.post("/login", login_user_handler)

// updating the user password
app.put("/forget", forgot_user_handler)




// this server is listening the 3000 port
app.listen(3001,()=>{
    console.log("Listening to port 3001")
})
