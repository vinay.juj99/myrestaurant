// import{Link} from 'react-router-dom'
import './login.css';
import React from 'react';
import loginApiCallFun from './loginapicall';

class LoginApp extends React.Component {
    state = {
        username:"",
        password:"",
        re_enter_password:"",
        email:"",
        security_question:"",
        name:"",
        isSignUp:false,
        isLogin:false,
        login_error_msg:"",
        error_msg:"",
        login_username:"",
        login_password:""
    }

    signUpSuccess = ()=>{
        const{history} = this.props
        history.push("/landingpage")
    }

    apiCallFail = (data)=>{
        this.setState({isSignUp:true,error_msg:data.status_message})
    }

    loginFail = (data)=>{
        this.setState({isLogin:true,login_error_msg:data.status_message})
    }

    loginApiCall = ()=>{
        const{login_username,login_password} = this.state
        loginApiCallFun(login_username,login_password)
    }

    signupApiCall = async()=>{
        const{name,username,password,re_enter_password,email,security_question} = this.state
        const url = "http://localhost:3001/register"
        const userDetails = {
            name,
            username,
            password,
            re_enter_password,
            email,
            security_question,
        }
        const option = {
            method:"POST",
            body: JSON.stringify(userDetails),
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        }
        const response = await fetch(url,option)
        const data = await response.json()
        if (data.status_code === 200){
            this.signUpSuccess()
        }
        else{
            this.apiCallFail(data)
        }
    }

    changeUsername = (event)=>{
        this.setState({username:event.target.value})
    }

    loginUsernameChange = (event)=>{
        this.setState({login_username:event.target.value})
    }

    loginPasswordChange = (event)=>{
        this.setState({login_password:event.target.value})
    }

    changeName = (event)=>{
        this.setState({name:event.target.value})
    }
    changePassword = (event)=>{
        this.setState({password:event.target.value})
    }
    changeReEnterPassword = (event)=>{
        this.setState({re_enter_password:event.target.value})
    }
    changeEmail = (event)=>{
        this.setState({email:event.target.value})
    }
    changeSecurity = (event)=>{
        this.setState({security_question:event.target.value})
    }
    render() {
        const{isSignUp,error_msg,isLogin,login_error_msg} = this.state
    return (
    <div>
        <div>
            <h1 class="heading">Welcome to Our Website</h1>
        </div>
        <div class="login-wrap">
            <div class="login-html">
                <input id="tab-1" type="radio" name="tab" class="sign-in" /><label for="tab-1" class="tab">Sign In</label>
                <input id="tab-2" type="radio" name="tab" class="sign-up"/><label for="tab-2" class="tab">Sign Up</label>
                <div class="login-form">
                    <div class="sign-in-htm">
                        <div class="group">
                            <label for="user" class="label">Username</label>
                            <input id="user" type="text" class="input" onChange={this.loginUsernameChange}/>
                        </div>
                        <div class="group">
                            <label for="pass" class="label">Password</label>
                            <input id="pass" type="password" class="input" data-type="password" onChange={this.loginPasswordChange}/>
                        </div>
                        <div class="group">
                            <input id="check" type="checkbox" class="check" />
                            <label for="check"><span class="icon"></span> Keep me Signed in</label>
                        </div>
                        <div class="group">
                          <button class="button" onClick={this.loginApiCall}>Sign In</button>
                        </div>
                        {isLogin && <p>{login_error_msg}</p>}
                        <div class="hr"></div>
                        <div class="foot-lnk">
                            <a href="./forgotpassword">Forgot Password?</a>
                        </div>
                    </div>
                    <div class="sign-up-htm">
                    <div class="group">
                            <label for="user" class="label">name</label>
                            <input id="user1" type="text" class="input" onChange={this.changeName}/>
                        </div>
                        <div class="group">
                            <label for="user" class="label">Username</label>
                            <input id="user2" type="text" class="input" onChange={this.changeUsername}/>
                        </div>
                        <div class="group">
                            <label for="pass" class="label">Password</label>
                            <input id="pass1" type="password" class="input" data-type="password" onChange={this.changePassword}/>
                        </div>
                        <div class="group">
                            <label for="pass" class="label">Repeat Password</label>
                            <input id="pass2" type="password" class="input" data-type="password" onChange={this.changeReEnterPassword}/>
                        </div>
                        <div class="group">
                            <label for="pass" class="label">Email Address</label>
                            <input id="pass3" type="text" class="input" onChange={this.changeEmail}/>
                        </div>
                        <div class="group">
                            <label for="pass" class="label">Security Question</label>
                            <input id="pass4" type="text" class="input" onChange={this.changeSecurity}/>
                        </div>
                        <div class="group">
                            <button className='button' type='button' onClick={this.signupApiCall}>SignUp</button>
                        </div>
                        {isSignUp && <p>{error_msg}</p>}
                        <div class="hr"></div>
                        <div class="foot-lnk">
                            <a href="/#" for="tab-1">Already Member?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
}
}

export default LoginApp;
