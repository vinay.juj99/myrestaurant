const footer_data = {
    img:"./images/landingpage/footer/images.png",
    img1:"./images/landingpage/footer/hyderabadi-dum-biryani.jpg",
    img2:"./images/landingpage/footer/attractive-young-couple-sharing-food-cute-hispanic-having-good-time-together-coffee-shop-68073376.jpg"
}

const header_data = {
    img3:"./images/landingpage/header/tranquil-escape.jpg"
}

const about_data = {
    img4:"./images/menu/about/images.png"
}

const contact_data = {
    img5:"./images/menu/contact/restraunt2.jpg"
}

const food_data = {
    img6:"./images/menu/food/160802_chinese_food.jpg",
    img7:"./images/menu/food/Chicken-Biryani.jpg",
    img8:"./images/menu/food/wedding-menu-food.jpg"
}

const foodmenu_data = {
    img9:"./images/landingpage/footer/hyderabadi-dum-biryani.jpg",
    img10:"./images/menu/foodmenu/chicken.seekh.jpg",
    img11:"./images/menu/foodmenu/istockphoto-483120255-612x612.jpg",
    img12:"./images/menu/foodmenu/personalised-mix-non.jpg"
    
}

const testimonials_data = {
    img14:"./images/menu/testimonals/male-photo1.jpg",
    img15:"./images/menu/testimonals/female-photo1.jpg",
    img16:"./images/menu/testimonals/male-photo3.jpg"
}
const category_data = {

    img17:"./images/menu/category/wedding-menu-food.jpg",

    img18:"./images/menu/category/160802_chinese_food.jpg",

    img19:"./images/menu/category/wedding-menu-food.jpg"

}

export {footer_data, header_data, contact_data,category_data, foodmenu_data, testimonials_data, about_data, food_data};
