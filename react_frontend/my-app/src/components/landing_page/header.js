
import './landing_page.css';

import react from 'react';

class Header extends react.Component {
  render() {
    console.log(this.props)
    const { header_data } = this.props
    return (
      <div>
        <img src={header_data.img3} alt="tranquil-escape-logo" border="0" />

        <h1>Restaurent Rating Delivery</h1>

        <p>713 Lithia Pinecrest Drive, Brandon, FL 33511</p>
      </div>
    );
  }
}

export default Header;
