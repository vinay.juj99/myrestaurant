import './landing_page.css';
import Header from './header.js';
import Footer from './footer';
import { header_data } from '../images data/data';
import { footer_data } from '../images data/data';
import React from 'react';
const LandingPageApp = ()=> {
    return (
            <>
            <Header header_data={header_data}/>
            <Footer footer_data={footer_data}/>
            </>
    )
}
export default LandingPageApp;
