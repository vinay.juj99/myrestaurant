

import React from 'react';
import './landing_page.css';
import { Link } from 'react-router-dom';
class Footer extends React.Component {
    render() {
        console.log(this.props)
        const { footer_data } = this.props
        return (
            <section class="parallax">
                <div class="container center">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2>Our Restaurents:</h2>
                            <p>Note:Use Dineout Coupon 50% Off to redeem a discount via Dineout Passport Membership. Applicable across more than 2000 restaurants.</p>
                        </div>
                    </div>

                    <div id="container">
                    

                        <div class="product-details">


                            <h1>Spicy_Kitchen</h1>

                            <p class="information">Based on reviwes from<strong> 385 coustomers</strong></p>


                            <div class="container">
                                <ul class="rating threestar">
                                    <li class="one"><a href="/#" title="1 Star">1</a></li>
                                    <li class="two"><a href="/#" title="2 Stars">2</a></li>
                                    <li class="three"><a href="/#" title="3 Stars">3</a></li>
                                    <li class="four"><a href="/#" title="4 Stars">4</a></li>
                                    <li class="five"><a href="/#" title="5 Stars">5</a></li>
                                </ul>

                            </div>




                            <p class="information">Based on reviwes from<strong> 385 coustomers</strong></p>


                            <div class="control">
                                <Link to='/menupage'>
                                    <button class="btn">
                                        {/*<!-- 		the Price -->
                            <!-- 		shopping cart icon-->
                            <!-- 		Buy Now / ADD to Cart-->*/}
                                        Buy Now
                                    </button></Link>


                            </div>

                        </div>



                        <div class="product-image">

                            <img src={footer_data.img} alt="" />

                            <div class="info">
                                <h2>Menu</h2>
                                <ul>
                                    <li><strong>Phaal Curry: </strong>320/-</li>
                                    <li><strong>Lall Maasal: </strong>190/-</li>
                                    <li><strong>Chiken Chettinad: </strong>490/-</li>
                                    <li><strong>Rista: </strong>250/-</li>
                                    <li><strong>Kozi Kari: </strong>220/-</li>

                                </ul>
                            </div>
                        </div>


                    </div>

                    <div id="container">

                        <div class="product-details">
                            <h1>Delicious_hut</h1>
                            <p class="information">Based on reviwes from<strong>555 coustomers</strong></p>
                            <div class="container">
                                <ul class="rating fourstar">
                                    <li class="one"><a href="/#" title="1 Star">1</a></li>
                                    <li class="two"><a href="/#" title="2 Stars">2</a></li>
                                    <li class="three"><a href="/#" title="3 Stars">3</a></li>
                                    <li class="four"><a href="/#" title="4 Stars">4</a></li>
                                    <li class="five"><a href="/#" title="5 Stars">5</a></li>
                                </ul>

                            </div>




                            <div class="control">
                                <Link to='/menupage'>
                                    <button class="btn">


                                        Buy Now
                                    </button></Link>

                            </div>

                        </div>

                        <div class="product-image">

                            <img
                                src={footer_data.img1} alt="" />


                            <div class="info">
                                <h2>Menu</h2>
                                <ul>
                                    <li><strong>Shutki Maach: </strong>370/-</li>
                                    <li><strong>Chicken 65: </strong>199/-</li>
                                    <li><strong>Kolhapuri hicken: </strong>590/-</li>
                                    <li><strong>Beef Chill Fry: </strong>260/-</li>
                                    <li><strong>Pork Vindaloo: </strong>290/-</li>
                                </ul>
                            </div>
                        </div>



                    </div>

                    <div id="container">

                        <div class="product-details">

                            <h1>Couples_hub</h1>
                            <p class="information">Based on reviwes from <strong>455 coustomers</strong></p>

                            <div class="container">
                                <ul class="rating fourstar">
                                    <li class="one"><a href="/#" title="1 Star">1</a></li>
                                    <li class="two"><a href="/#" title="2 Stars">2</a></li>
                                    <li class="three"><a href="/#" title="3 Stars">3</a></li>
                                    <li class="four"><a href="/#" title="4 Stars">4</a></li>
                                    <li class="five"><a href="/#" title="5 Stars">5</a></li>
                                </ul>

                            </div>



                            <div class="control">

                                <Link to='/menupage'>
                                    <button class="btn">

                                        Buy Now
                                    </button></Link>


                            </div>

                        </div>

                        <div class="product-image">

                            <img
                                src={footer_data.img2} alt="" />

                            <div class="info">
                                <h2>Menu</h2>
                                <ul>
                                    <li><strong>Thandoori Chiken: </strong>370/-</li>
                                    <li><strong>Prawns Curry: </strong>199/-</li>
                                    <li><strong>Aapolo Fish: </strong>590/-</li>
                                    <li><strong>Thai curry: </strong>260/-</li>
                                    <li><strong>Chiken Pakodi: </strong>290/-</li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Footer;
