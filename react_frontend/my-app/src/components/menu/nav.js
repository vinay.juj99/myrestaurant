
import react from 'react';
import './menu.css';

class Navbar extends react.Component {
  render() {
  return (
    <nav className="navbar">
      <div className="navbar-container container">
          <input type="checkbox" name="" id=""/>
          <div className="hamburger-lines">
              <span className="line line1"></span>
              <span className="line line2"></span>
              <span className="line line3"></span>
          </div>
          <ul className="menu-items">
          <h1>RS</h1>
              <li><a href="#home">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#food">Category</a></li>
              <li><a href="#food-menu">Menu</a></li>
              <li><a href="#testimonials">Testimonial</a></li>
              <li><a href="#contact">Contact</a></li>
          </ul>
          
      </div>
  </nav>
  );
}
}

export default Navbar;
