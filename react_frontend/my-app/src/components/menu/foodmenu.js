
import React from 'react';
import './menu.css';

class Foodmenu extends React.Component {
  render() {
    /*const {foodmenu_data} = this.props*/
  return (
    <section id="food-menu">
    <h2 className="food-menu-heading">Food Menu</h2>
    <div className="food-menu-container container">
      <div className="food-menu-item">
        <div className="food-img">
          <img src="https://www.mistay.in/travel-blog/content/images/2020/07/hyderbadi-biriyani-1.jpg" alt="" />
        </div>
        <div className="food-description">
          <h2 className="food-titile">Food Menu Item 1</h2>
          <p>
            Hyderabadi biryani, also known as Hyderabadi dum biryani, is a style of biryani from Hyderabad, India made with basmati rice and goat meat .
          </p>
          <p className="food-price">Price: &#8377; 250</p>
        </div>
      </div>

      <div className="food-menu-item">
        <div className="food-img">
          <img
            src="https://media.istockphoto.com/photos/asian-oranage-chicken-with-green-onions-picture-id483120255?k=20&m=483120255&s=612x612&w=0&h=UX3BT-1WCvwAdsSiuuKTeFjklOdPXndZFwFQ0AeJyGE="
            alt="error"
          />
        </div>
        <div className="food-description">
          <h2 className="food-titile">Food Menu Item 2</h2>
          <p>
            Better to be deprived of food for three days, than tea for one." ... "To the ruler, the people are heaven; to the people, food is heaven.
          </p>
          <p className="food-price">Price: &#8377; 250</p>
        </div>
      </div>
      <div className="food-menu-item">
        <div className="food-img">
          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRo1JzQd1f25Vs3drGXqgXRyQdniV8NKCRE3w&usqp=CAU" alt="" />
        </div>
        <div className="food-description">
          <h2 className="food-titile">Food Menu Item 3</h2>
          <p>
            The vegetarian dishes consist of koora, which include cooking different vegetables in a variety of styles - with gravy, frying, with lentils, etc.
          </p>
          <p className="food-price">Price: &#8377; 110</p>
        </div>
      </div>
      <div className="food-menu-item">
        <div className="food-img">
          <img src="https://media-cdn.tripadvisor.com/media/photo-s/19/72/19/d0/personalised-mix-non.jpg" alt="" />
        </div>
        <div className="food-description">
          <h2 className="food-titile">Food Menu Item 4</h2>
          <p>
            This collection of tempting non vegetarian starters and snacks comes with the promise to tantalise the taste buds just so. . . Reading books is a kind of enjoyment. Reading books is a good habit
          </p>
          <p className="food-price">Price: &#8377; 349</p>
        </div>
      </div>
      <div className="food-menu-item">
        <div className="food-img">
          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQP_yjn08R5HmW3fy0zYfeAJKTCA51E0fSOjw&usqp=CAU" alt="" />
        </div>
        <div className="food-description">
          <h2 className="food-titile">Food Menu Item 5</h2>
          <p>
            The Afghani Chicken Kebabs are made from minced meat of boneless tender cuts, and seasoned with spices and herbs
          </p>
          <p className="food-price">Price: &#8377; 199</p>
        </div>
      </div>
      <div className="food-menu-item">
        <div className="food-img">
          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxhck4k1v_JXthaywSzF3BhWy_JY8MZsqR3w&usqp=CAU" alt="" />
        </div>
        <div className="food-description">
          <h2 className="food-titile">Food Menu Item 6</h2>
          <p>
            Really fun cocktail class with Kay - really focused on the details of every drink, learnt a lot of small tips and tricks of the trade. Cocktails were great too.
          </p>
          <p className="food-price">Price: &#8377; 249</p>
        </div>
      </div>
    </div>
  </section>
  );
}
}

export default Foodmenu;
