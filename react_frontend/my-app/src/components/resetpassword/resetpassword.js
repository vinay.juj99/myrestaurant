import './resetpassword.css';
import React from 'react';
function ResetPasswordApp() {
    return(
        <div class="reset-password-box">

            <div class="title-bar">
              <div class="title">PASSWORD RESET</div>
          
            </div>
          
          
            <div class="new-password">
              <label for="new-password-input" class="new-password-label">New Password</label>
              <input type="password" id="new-password-input"/>
            </div>
          
            <div class="password-verification">
              <label for="password-input" class="password--verification-label">Password Verification</label>
              <input type="password" id="password-verification-input"/>
            </div>
          
          
            <div class="back-login">
              <div class="reset-password-button" onclick="airTableResetPassword()">
                
                <a type='submit' href='/#'>Reset Password</a>
              </div>
            </div>
          
          </div>
    )
}
export default ResetPasswordApp;
