import{Link} from 'react-router-dom'
import './otp.css';
import React from 'react';

function Otp(){
    return(
        <div class="container">
		<h1>ENTER OTP</h1>
		<div class="userInput">
			<input type="text" id='ist' maxlength="1" onkeyup="clickEvent(this,'sec')"/>
			<input type="text" id="sec" maxlength="1" onkeyup="clickEvent(this,'third')"/>
			<input type="text" id="third" maxlength="1" onkeyup="clickEvent(this,'fourth')"/>
			<input type="text" id="fourth" maxlength="1" onkeyup="clickEvent(this,'fifth')"/>
			<input type="text" id="fifth" maxlength="1"/>
		</div>
		<Link to='/resetpassword'>
		<button>CONFIRM</button></Link>
	</div>
    );
}
export default Otp;