import React from 'react';
import { BrowserRouter, Route, Switch} from "react-router-dom";

import LoginApp from './components/loginIn/login';
import Forgotpass from './components/forgotPass/forgotpass';
import Otp from './components/otp/otp';
import ResetPasswordApp from './components/resetpassword/resetpassword';
import LandingPageApp from './components/landing_page/landingpageApp';
import MenuApp from './components/menu/menuApp';
import Menu2app from './components/menu2/Menu2app';
import Menu3app from './components/menu3/Menu3app';

function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route path='/' exact component= {LoginApp}/>
      <Route path='/forgotpassword' exact component={Forgotpass}/>
      <Route path='/otp' exact component ={Otp}/>
      <Route path='/resetpassword' exact component={ResetPasswordApp}/>
      <Route path='/landingpage' exact component= {LandingPageApp}/>
      <Route path='/menupage1' exact component= {MenuApp}/>
      <Route path='/menupage2' exact component={Menu2app}/>
      <Route path='/menupage' exact component={Menu3app}/>
    </Switch>
    </BrowserRouter>
  );
}

export default App;
